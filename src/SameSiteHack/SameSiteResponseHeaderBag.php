<?php
namespace Esc\Shopify\SameSiteHack;


/**
 * This class patches in support for SameSite=None in Laravel pre-5.5
 * For newer versions where SameSite=None is supported, this isn't used.
 */

class SameSiteResponseHeaderBag {
    public $__bag;

    public function __construct($bag) {
        $this->__bag = $bag;
    }

    public function __call($method, $args) {
        $r = call_user_func_array([$this->__bag, $method], $args);
        if ($method == 'getCookies') {
            // Intercept getCookies to replace Cookie class with
            // proxy obj that handles __toString
            return array_map(function($cookie) {
                return new SameSiteCookie($cookie);
            }, $r);
        }
        return $r;
    }
    public function __get($key) {
        return $this->__bag[$key];
    }
    public function __set($key, $value){
        return $this->__bag[$key] = $value;
    }
    public function __isset($key) {
        return isset($this->__bag[$key]);
    }
}
