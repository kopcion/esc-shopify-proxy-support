<?php
namespace Esc\Shopify;

class EASDK {
    private $api;

    public function __construct() {
        $api = app('ShopifyAPI');
    }

    public function getAPI() {
        return $this->api;
    }

    public function setAPI($api) {
        $this->api = $api;
    }

    public function hostedRedirect($shopDomain, $url) {
        $shopDomainString = json_encode('https://' . $shopDomain);
        $urlString = json_encode($url);

        return <<<EOF
<!DOCTYPE html>
<html>
<head>
</head>
<body>
    <script type="text/javascript">
    if (window.top == window.self) { 
        window.top.location.href = $urlString; 
    } else { 
        message = JSON.stringify({ 
            message: "Shopify.API.remoteRedirect", 
            data: { 
                location:  $urlString 
            },
        }); 
        window.parent.postMessage(message, $shopDomainString); 
    } 
    </script>
</body>
</html>
EOF;
    }
}
